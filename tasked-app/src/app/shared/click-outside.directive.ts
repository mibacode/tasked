/**
 * https://christianliebel.com/2016/05/angular-2-a-simple-click-outside-directive/
 * 
 * Note:
 * You must use *ngIf (not [hidden]) because:
 *    1. This is the only way to know when the actual initial click is.
 *    2. This event will be called everytime a user click outside of the
 *        element, even if it is hidden. 
 */

import { Directive, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[clickOutside]'
})
export class ClickOutsideDirective {

  constructor(private elementRef : ElementRef) { }

  private initialClick:boolean = true; 

  @Output()
  public clickOutside = new EventEmitter();

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
      if (!clickedInside) {
          this.clickOutside.emit(this.initialClick);
          this.initialClick = false;
      }
  }
}
