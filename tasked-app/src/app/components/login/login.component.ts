import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import * as firebase from 'firebase';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private isLoggedIn: boolean = false;

  constructor(public router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  login() {
    this.authService.login()
    .then(auth => {
      if (auth !== null) {
        this.isLoggedIn = true;
         this.router.navigate(['daily']);
      }
      else {
        this.isLoggedIn = false;
      }
    })
    .catch((err) => {
      console.log("User probably closed the login popup page.");
    });
  }

  logout() {
    this.authService.logout()
    .then(() => {
      this.isLoggedIn = false;
    })
  }

}
