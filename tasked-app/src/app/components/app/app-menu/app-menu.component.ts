import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import * as firebase from 'firebase';

import { Observable } from 'rxjs/Observable';

import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './app-menu.component.html',
  styleUrls: ['./app-menu.component.scss']
})
export class AppMenuComponent implements OnInit {

  private isLoggedIn: boolean;
  private expanded: boolean = false;
  private menu = [{
    title: "Dashboard",
    icon: "dashboard",
    location: "/dashboard"
  },
  {
    title: "Daily Tasks",
    icon: "today",
    location: "/daily"
  }]

  constructor(private authService: AuthService, private router: Router) {
    firebase.auth()
    .onAuthStateChanged((auth) => {
        this.expanded = false;
        this.isLoggedIn = (auth !== null);
        console.log("AUTH: "+ this.isLoggedIn);
      }
    );
  }

  ngOnInit() {

  }

  close(initialClick) {
    if (!initialClick) {
      this.expanded = false;
    }
  }

  login() {
    this.authService.login()
    .then((auth) => {
      if (auth) {
        this.router.navigate(['dashboard']);
      }
    });
  }

  logout() {
    this.authService.logout();
  }

}
