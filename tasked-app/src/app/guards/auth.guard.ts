import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean
  {
    return new Promise((resolve, reject) => {
      this.authService
      .isLoggedIn()
      .then(isLoggedIn => {
        if (!isLoggedIn) {
          this.router.navigate(['login']);
        }
        resolve(isLoggedIn);
      });
    });
  }
}
