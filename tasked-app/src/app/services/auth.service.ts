import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

import * as firebase from 'firebase';

@Injectable()
export class AuthService {

  private loggedIn: boolean = false;

  constructor(private router: Router) { }

  isLoggedIn(): Promise<boolean> {
    return new Promise(function(resolve, reject) {
      var obs = firebase.auth()
      .onAuthStateChanged((auth) => {
        if (auth === null) {
          obs();
          resolve(false);
        }
        else {
          obs();
          resolve(true);
        }
      });
    });
  }

  login(): firebase.Promise<any> {
      return this.loginWithGoogle();
  }

  loginWithGoogle(): firebase.Promise<any> {
		return firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider());
	}

	/**
	 * Logs out the current user
	 */
	logout() {
    return firebase.auth().signOut()
    .then(() => {
      this.router.navigate(['login']);
    });
  }
}
