import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';

import { AppRouting } from './app.routing'; 

import { AppComponent } from './components/app/app.component';
import { DailyPlannerComponent } from './components/daily-planner/daily-planner.component';
import { AppMenuComponent } from './components/app/app-menu/app-menu.component';
import { LoginComponent } from './components/login/login.component';

import { AuthGuard } from './guards/auth.guard';

import { AuthService } from './services/auth.service';
import { ClickOutsideDirective } from './shared/click-outside.directive';
import { BaseComponent } from './components/base/base.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FrontPageComponent } from './components/front-page/front-page.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    DailyPlannerComponent,
    AppMenuComponent,
    LoginComponent,
    ClickOutsideDirective,
    BaseComponent,
    DashboardComponent,
    FrontPageComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AppRouting
  ],
  providers: [AuthGuard, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
