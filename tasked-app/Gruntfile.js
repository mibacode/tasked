module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                options: {
                    sourcemap: 'none'
                },
                files: [{
                    expand: true,
                    cwd: 'src/assets/scss',
                    src: ['**/*.scss'],
                    dest: 'src/assets/css',
                    ext: '.css'
                }]
            }
        },

        postcss: { // Begin Post CSS Plugin
            options: {
                map: false,
                processors: [
                    require('autoprefixer')({
                        browsers: ['last 2 versions']
                    })
                ]
            },
            dist: {
                src: 'src/assets/css/style.css'
            }
        },

        cssmin: { // Begin CSS Minify Plugin
            target: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['src/assets/css/*.css', '!*.min.css'],
                    dest: 'src/assets/css',
                    ext: '.min.css'
                }]
            }
        },

        uglify: { // Begin JS Uglify Plugin
            build: {
                src: ['src/js/*.js'],
                dest: 'assets/js/script.min.js'
            }
        },

        watch: { // Compile everything into one task with Watch Plugin
            css: {
                files: 'src/assets/scss/**/*.scss',
                tasks: ['sass'] //, 'postcss', 'cssmin'
            },
            // js: {
            //     files: 'src/js/**/*.js',
            //     tasks: ['uglify']
            // }
        }
	});

        // Load Grunt plugins
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Register Grunt tasks
	grunt.registerTask('default', ['watch']);
}