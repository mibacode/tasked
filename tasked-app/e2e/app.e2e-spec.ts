import { TaskedAppPage } from './app.po';

describe('tasked-app App', () => {
  let page: TaskedAppPage;

  beforeEach(() => {
    page = new TaskedAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
